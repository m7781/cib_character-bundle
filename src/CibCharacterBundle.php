<?php

namespace Cib\CharacterBundle\CibCharacterBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class CibCharacterBundle extends Bundle
{

    public function getPath(): string
    {
        return \dirname(__DIR__);
    }
}